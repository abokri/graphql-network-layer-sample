class GraphQLQueryWrapper<Data> {
  final DocumentNode document;
  final QueryOptions<Data> options;

  GraphQLQueryWrapper(this.document, this.options);
}
