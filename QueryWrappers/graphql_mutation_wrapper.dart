class GraphQLMutationWrapper<Data> {
  final DocumentNode document;
  final MutationOptions<Data> options;

  GraphQLMutationWrapper(this.document, this.options);
}
