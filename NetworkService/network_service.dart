class NetworkService implements INetworkService {
  final GraphQLClient _graphQLClient;

  NetworkService(this._graphQLClient);

  Future<TData> query<TData>(GraphQLQueryWrapper<TData> queryWrapper) async {
    final result = await _graphQLClient.query<TData>(
      queryWrapper.options,
    );

    if (result.hasException) {
      // Handle exception
      throw result.exception!;
    }

    return result.parsedData!;
  }

  Future<TData> mutation<TData>(GraphQLMutationWrapper<TData> queryWrapper) async {
    final result = await _graphQLClient.mutate<TData>(
      queryWrapper.options,
    );

    if (result.hasException) {
      // Handle exception
      throw result.exception!;
    }

    return result.parsedData!;
  }
}
