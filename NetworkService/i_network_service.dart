abstract class INetworkService {
  Future<TData> query<TData>(GraphQLQueryWrapper<TData> queryWrapper);
  Future<TData> mutation<TData>(GraphQLMutationWrapper<TData> queryWrapper);
}
