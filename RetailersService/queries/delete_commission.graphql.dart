import '../schema/retailers_schema.graphql.dart';
import 'dart:async';
import 'package:flutter/widgets.dart' as widgets;
import 'package:gql/ast.dart';
import 'package:graphql/client.dart' as graphql;
import 'package:graphql_flutter/graphql_flutter.dart' as graphql_flutter;

class Variables$Mutation$DeleteCommission {
  factory Variables$Mutation$DeleteCommission(
          {required Input$DeleteCommissionDto deleteCommissionDto}) =>
      Variables$Mutation$DeleteCommission._({
        r'deleteCommissionDto': deleteCommissionDto,
      });

  Variables$Mutation$DeleteCommission._(this._$data);

  factory Variables$Mutation$DeleteCommission.fromJson(
      Map<String, dynamic> data) {
    final result$data = <String, dynamic>{};
    final l$deleteCommissionDto = data['deleteCommissionDto'];
    result$data['deleteCommissionDto'] = Input$DeleteCommissionDto.fromJson(
        (l$deleteCommissionDto as Map<String, dynamic>));
    return Variables$Mutation$DeleteCommission._(result$data);
  }

  Map<String, dynamic> _$data;

  Input$DeleteCommissionDto get deleteCommissionDto =>
      (_$data['deleteCommissionDto'] as Input$DeleteCommissionDto);

  Map<String, dynamic> toJson() {
    final result$data = <String, dynamic>{};
    final l$deleteCommissionDto = deleteCommissionDto;
    result$data['deleteCommissionDto'] = l$deleteCommissionDto.toJson();
    return result$data;
  }

  CopyWith$Variables$Mutation$DeleteCommission<
          Variables$Mutation$DeleteCommission>
      get copyWith => CopyWith$Variables$Mutation$DeleteCommission(
            this,
            (i) => i,
          );

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) {
      return true;
    }
    if (!(other is Variables$Mutation$DeleteCommission) ||
        runtimeType != other.runtimeType) {
      return false;
    }
    final l$deleteCommissionDto = deleteCommissionDto;
    final lOther$deleteCommissionDto = other.deleteCommissionDto;
    if (l$deleteCommissionDto != lOther$deleteCommissionDto) {
      return false;
    }
    return true;
  }

  @override
  int get hashCode {
    final l$deleteCommissionDto = deleteCommissionDto;
    return Object.hashAll([l$deleteCommissionDto]);
  }
}

abstract class CopyWith$Variables$Mutation$DeleteCommission<TRes> {
  factory CopyWith$Variables$Mutation$DeleteCommission(
    Variables$Mutation$DeleteCommission instance,
    TRes Function(Variables$Mutation$DeleteCommission) then,
  ) = _CopyWithImpl$Variables$Mutation$DeleteCommission;

  factory CopyWith$Variables$Mutation$DeleteCommission.stub(TRes res) =
      _CopyWithStubImpl$Variables$Mutation$DeleteCommission;

  TRes call({Input$DeleteCommissionDto? deleteCommissionDto});
}

class _CopyWithImpl$Variables$Mutation$DeleteCommission<TRes>
    implements CopyWith$Variables$Mutation$DeleteCommission<TRes> {
  _CopyWithImpl$Variables$Mutation$DeleteCommission(
    this._instance,
    this._then,
  );

  final Variables$Mutation$DeleteCommission _instance;

  final TRes Function(Variables$Mutation$DeleteCommission) _then;

  static const _undefined = <dynamic, dynamic>{};

  TRes call({Object? deleteCommissionDto = _undefined}) =>
      _then(Variables$Mutation$DeleteCommission._({
        ..._instance._$data,
        if (deleteCommissionDto != _undefined && deleteCommissionDto != null)
          'deleteCommissionDto':
              (deleteCommissionDto as Input$DeleteCommissionDto),
      }));
}

class _CopyWithStubImpl$Variables$Mutation$DeleteCommission<TRes>
    implements CopyWith$Variables$Mutation$DeleteCommission<TRes> {
  _CopyWithStubImpl$Variables$Mutation$DeleteCommission(this._res);

  TRes _res;

  call({Input$DeleteCommissionDto? deleteCommissionDto}) => _res;
}

class Mutation$DeleteCommission {
  Mutation$DeleteCommission({
    this.deleteCommission,
    this.$__typename = 'Mutation',
  });

  factory Mutation$DeleteCommission.fromJson(Map<String, dynamic> json) {
    final l$deleteCommission = json['deleteCommission'];
    final l$$__typename = json['__typename'];
    return Mutation$DeleteCommission(
      deleteCommission: (l$deleteCommission as bool?),
      $__typename: (l$$__typename as String),
    );
  }

  final bool? deleteCommission;

  final String $__typename;

  Map<String, dynamic> toJson() {
    final _resultData = <String, dynamic>{};
    final l$deleteCommission = deleteCommission;
    _resultData['deleteCommission'] = l$deleteCommission;
    final l$$__typename = $__typename;
    _resultData['__typename'] = l$$__typename;
    return _resultData;
  }

  @override
  int get hashCode {
    final l$deleteCommission = deleteCommission;
    final l$$__typename = $__typename;
    return Object.hashAll([
      l$deleteCommission,
      l$$__typename,
    ]);
  }

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) {
      return true;
    }
    if (!(other is Mutation$DeleteCommission) ||
        runtimeType != other.runtimeType) {
      return false;
    }
    final l$deleteCommission = deleteCommission;
    final lOther$deleteCommission = other.deleteCommission;
    if (l$deleteCommission != lOther$deleteCommission) {
      return false;
    }
    final l$$__typename = $__typename;
    final lOther$$__typename = other.$__typename;
    if (l$$__typename != lOther$$__typename) {
      return false;
    }
    return true;
  }
}

extension UtilityExtension$Mutation$DeleteCommission
    on Mutation$DeleteCommission {
  CopyWith$Mutation$DeleteCommission<Mutation$DeleteCommission> get copyWith =>
      CopyWith$Mutation$DeleteCommission(
        this,
        (i) => i,
      );
}

abstract class CopyWith$Mutation$DeleteCommission<TRes> {
  factory CopyWith$Mutation$DeleteCommission(
    Mutation$DeleteCommission instance,
    TRes Function(Mutation$DeleteCommission) then,
  ) = _CopyWithImpl$Mutation$DeleteCommission;

  factory CopyWith$Mutation$DeleteCommission.stub(TRes res) =
      _CopyWithStubImpl$Mutation$DeleteCommission;

  TRes call({
    bool? deleteCommission,
    String? $__typename,
  });
}

class _CopyWithImpl$Mutation$DeleteCommission<TRes>
    implements CopyWith$Mutation$DeleteCommission<TRes> {
  _CopyWithImpl$Mutation$DeleteCommission(
    this._instance,
    this._then,
  );

  final Mutation$DeleteCommission _instance;

  final TRes Function(Mutation$DeleteCommission) _then;

  static const _undefined = <dynamic, dynamic>{};

  TRes call({
    Object? deleteCommission = _undefined,
    Object? $__typename = _undefined,
  }) =>
      _then(Mutation$DeleteCommission(
        deleteCommission: deleteCommission == _undefined
            ? _instance.deleteCommission
            : (deleteCommission as bool?),
        $__typename: $__typename == _undefined || $__typename == null
            ? _instance.$__typename
            : ($__typename as String),
      ));
}

class _CopyWithStubImpl$Mutation$DeleteCommission<TRes>
    implements CopyWith$Mutation$DeleteCommission<TRes> {
  _CopyWithStubImpl$Mutation$DeleteCommission(this._res);

  TRes _res;

  call({
    bool? deleteCommission,
    String? $__typename,
  }) =>
      _res;
}

const documentNodeMutationDeleteCommission = DocumentNode(definitions: [
  OperationDefinitionNode(
    type: OperationType.mutation,
    name: NameNode(value: 'DeleteCommission'),
    variableDefinitions: [
      VariableDefinitionNode(
        variable: VariableNode(name: NameNode(value: 'deleteCommissionDto')),
        type: NamedTypeNode(
          name: NameNode(value: 'DeleteCommissionDto'),
          isNonNull: true,
        ),
        defaultValue: DefaultValueNode(value: null),
        directives: [],
      )
    ],
    directives: [],
    selectionSet: SelectionSetNode(selections: [
      FieldNode(
        name: NameNode(value: 'deleteCommission'),
        alias: null,
        arguments: [
          ArgumentNode(
            name: NameNode(value: 'deleteCommissionDto'),
            value: VariableNode(name: NameNode(value: 'deleteCommissionDto')),
          )
        ],
        directives: [],
        selectionSet: null,
      ),
      FieldNode(
        name: NameNode(value: '__typename'),
        alias: null,
        arguments: [],
        directives: [],
        selectionSet: null,
      ),
    ]),
  ),
]);
Mutation$DeleteCommission _parserFn$Mutation$DeleteCommission(
        Map<String, dynamic> data) =>
    Mutation$DeleteCommission.fromJson(data);
typedef OnMutationCompleted$Mutation$DeleteCommission = FutureOr<void> Function(
  Map<String, dynamic>?,
  Mutation$DeleteCommission?,
);

class Options$Mutation$DeleteCommission
    extends graphql.MutationOptions<Mutation$DeleteCommission> {
  Options$Mutation$DeleteCommission({
    String? operationName,
    required Variables$Mutation$DeleteCommission variables,
    graphql.FetchPolicy? fetchPolicy,
    graphql.ErrorPolicy? errorPolicy,
    graphql.CacheRereadPolicy? cacheRereadPolicy,
    Object? optimisticResult,
    Mutation$DeleteCommission? typedOptimisticResult,
    graphql.Context? context,
    OnMutationCompleted$Mutation$DeleteCommission? onCompleted,
    graphql.OnMutationUpdate<Mutation$DeleteCommission>? update,
    graphql.OnError? onError,
  })  : onCompletedWithParsed = onCompleted,
        super(
          variables: variables.toJson(),
          operationName: operationName,
          fetchPolicy: fetchPolicy,
          errorPolicy: errorPolicy,
          cacheRereadPolicy: cacheRereadPolicy,
          optimisticResult: optimisticResult ?? typedOptimisticResult?.toJson(),
          context: context,
          onCompleted: onCompleted == null
              ? null
              : (data) => onCompleted(
                    data,
                    data == null
                        ? null
                        : _parserFn$Mutation$DeleteCommission(data),
                  ),
          update: update,
          onError: onError,
          document: documentNodeMutationDeleteCommission,
          parserFn: _parserFn$Mutation$DeleteCommission,
        );

  final OnMutationCompleted$Mutation$DeleteCommission? onCompletedWithParsed;

  @override
  List<Object?> get properties => [
        ...super.onCompleted == null
            ? super.properties
            : super.properties.where((property) => property != onCompleted),
        onCompletedWithParsed,
      ];
}

class WatchOptions$Mutation$DeleteCommission
    extends graphql.WatchQueryOptions<Mutation$DeleteCommission> {
  WatchOptions$Mutation$DeleteCommission({
    String? operationName,
    required Variables$Mutation$DeleteCommission variables,
    graphql.FetchPolicy? fetchPolicy,
    graphql.ErrorPolicy? errorPolicy,
    graphql.CacheRereadPolicy? cacheRereadPolicy,
    Object? optimisticResult,
    Mutation$DeleteCommission? typedOptimisticResult,
    graphql.Context? context,
    Duration? pollInterval,
    bool? eagerlyFetchResults,
    bool carryForwardDataOnException = true,
    bool fetchResults = false,
  }) : super(
          variables: variables.toJson(),
          operationName: operationName,
          fetchPolicy: fetchPolicy,
          errorPolicy: errorPolicy,
          cacheRereadPolicy: cacheRereadPolicy,
          optimisticResult: optimisticResult ?? typedOptimisticResult?.toJson(),
          context: context,
          document: documentNodeMutationDeleteCommission,
          pollInterval: pollInterval,
          eagerlyFetchResults: eagerlyFetchResults,
          carryForwardDataOnException: carryForwardDataOnException,
          fetchResults: fetchResults,
          parserFn: _parserFn$Mutation$DeleteCommission,
        );
}

extension ClientExtension$Mutation$DeleteCommission on graphql.GraphQLClient {
  Future<graphql.QueryResult<Mutation$DeleteCommission>>
      mutate$DeleteCommission(
              Options$Mutation$DeleteCommission options) async =>
          await this.mutate(options);
  graphql.ObservableQuery<Mutation$DeleteCommission>
      watchMutation$DeleteCommission(
              WatchOptions$Mutation$DeleteCommission options) =>
          this.watchMutation(options);
}

class Mutation$DeleteCommission$HookResult {
  Mutation$DeleteCommission$HookResult(
    this.runMutation,
    this.result,
  );

  final RunMutation$Mutation$DeleteCommission runMutation;

  final graphql.QueryResult<Mutation$DeleteCommission> result;
}

Mutation$DeleteCommission$HookResult useMutation$DeleteCommission(
    [WidgetOptions$Mutation$DeleteCommission? options]) {
  final result = graphql_flutter
      .useMutation(options ?? WidgetOptions$Mutation$DeleteCommission());
  return Mutation$DeleteCommission$HookResult(
    (variables, {optimisticResult, typedOptimisticResult}) =>
        result.runMutation(
      variables.toJson(),
      optimisticResult: optimisticResult ?? typedOptimisticResult?.toJson(),
    ),
    result.result,
  );
}

graphql.ObservableQuery<Mutation$DeleteCommission>
    useWatchMutation$DeleteCommission(
            WatchOptions$Mutation$DeleteCommission options) =>
        graphql_flutter.useWatchMutation(options);

class WidgetOptions$Mutation$DeleteCommission
    extends graphql.MutationOptions<Mutation$DeleteCommission> {
  WidgetOptions$Mutation$DeleteCommission({
    String? operationName,
    graphql.FetchPolicy? fetchPolicy,
    graphql.ErrorPolicy? errorPolicy,
    graphql.CacheRereadPolicy? cacheRereadPolicy,
    Object? optimisticResult,
    Mutation$DeleteCommission? typedOptimisticResult,
    graphql.Context? context,
    OnMutationCompleted$Mutation$DeleteCommission? onCompleted,
    graphql.OnMutationUpdate<Mutation$DeleteCommission>? update,
    graphql.OnError? onError,
  })  : onCompletedWithParsed = onCompleted,
        super(
          operationName: operationName,
          fetchPolicy: fetchPolicy,
          errorPolicy: errorPolicy,
          cacheRereadPolicy: cacheRereadPolicy,
          optimisticResult: optimisticResult ?? typedOptimisticResult?.toJson(),
          context: context,
          onCompleted: onCompleted == null
              ? null
              : (data) => onCompleted(
                    data,
                    data == null
                        ? null
                        : _parserFn$Mutation$DeleteCommission(data),
                  ),
          update: update,
          onError: onError,
          document: documentNodeMutationDeleteCommission,
          parserFn: _parserFn$Mutation$DeleteCommission,
        );

  final OnMutationCompleted$Mutation$DeleteCommission? onCompletedWithParsed;

  @override
  List<Object?> get properties => [
        ...super.onCompleted == null
            ? super.properties
            : super.properties.where((property) => property != onCompleted),
        onCompletedWithParsed,
      ];
}

typedef RunMutation$Mutation$DeleteCommission
    = graphql.MultiSourceResult<Mutation$DeleteCommission> Function(
  Variables$Mutation$DeleteCommission, {
  Object? optimisticResult,
  Mutation$DeleteCommission? typedOptimisticResult,
});
typedef Builder$Mutation$DeleteCommission = widgets.Widget Function(
  RunMutation$Mutation$DeleteCommission,
  graphql.QueryResult<Mutation$DeleteCommission>?,
);

class Mutation$DeleteCommission$Widget
    extends graphql_flutter.Mutation<Mutation$DeleteCommission> {
  Mutation$DeleteCommission$Widget({
    widgets.Key? key,
    WidgetOptions$Mutation$DeleteCommission? options,
    required Builder$Mutation$DeleteCommission builder,
  }) : super(
          key: key,
          options: options ?? WidgetOptions$Mutation$DeleteCommission(),
          builder: (
            run,
            result,
          ) =>
              builder(
            (
              variables, {
              optimisticResult,
              typedOptimisticResult,
            }) =>
                run(
              variables.toJson(),
              optimisticResult:
                  optimisticResult ?? typedOptimisticResult?.toJson(),
            ),
            result,
          ),
        );
}
