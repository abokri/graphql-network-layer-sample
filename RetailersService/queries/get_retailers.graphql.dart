import 'dart:async';
import 'package:flutter/widgets.dart' as widgets;
import 'package:gql/ast.dart';
import 'package:graphql/client.dart' as graphql;
import 'package:graphql_flutter/graphql_flutter.dart' as graphql_flutter;

class Variables$Query$GetRetailers {
  factory Variables$Query$GetRetailers({
    required String clientId,
    required String country,
  }) =>
      Variables$Query$GetRetailers._({
        r'clientId': clientId,
        r'country': country,
      });

  Variables$Query$GetRetailers._(this._$data);

  factory Variables$Query$GetRetailers.fromJson(Map<String, dynamic> data) {
    final result$data = <String, dynamic>{};
    final l$clientId = data['clientId'];
    result$data['clientId'] = (l$clientId as String);
    final l$country = data['country'];
    result$data['country'] = (l$country as String);
    return Variables$Query$GetRetailers._(result$data);
  }

  Map<String, dynamic> _$data;

  String get clientId => (_$data['clientId'] as String);

  String get country => (_$data['country'] as String);

  Map<String, dynamic> toJson() {
    final result$data = <String, dynamic>{};
    final l$clientId = clientId;
    result$data['clientId'] = l$clientId;
    final l$country = country;
    result$data['country'] = l$country;
    return result$data;
  }

  CopyWith$Variables$Query$GetRetailers<Variables$Query$GetRetailers>
      get copyWith => CopyWith$Variables$Query$GetRetailers(
            this,
            (i) => i,
          );

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) {
      return true;
    }
    if (!(other is Variables$Query$GetRetailers) ||
        runtimeType != other.runtimeType) {
      return false;
    }
    final l$clientId = clientId;
    final lOther$clientId = other.clientId;
    if (l$clientId != lOther$clientId) {
      return false;
    }
    final l$country = country;
    final lOther$country = other.country;
    if (l$country != lOther$country) {
      return false;
    }
    return true;
  }

  @override
  int get hashCode {
    final l$clientId = clientId;
    final l$country = country;
    return Object.hashAll([
      l$clientId,
      l$country,
    ]);
  }
}

abstract class CopyWith$Variables$Query$GetRetailers<TRes> {
  factory CopyWith$Variables$Query$GetRetailers(
    Variables$Query$GetRetailers instance,
    TRes Function(Variables$Query$GetRetailers) then,
  ) = _CopyWithImpl$Variables$Query$GetRetailers;

  factory CopyWith$Variables$Query$GetRetailers.stub(TRes res) =
      _CopyWithStubImpl$Variables$Query$GetRetailers;

  TRes call({
    String? clientId,
    String? country,
  });
}

class _CopyWithImpl$Variables$Query$GetRetailers<TRes>
    implements CopyWith$Variables$Query$GetRetailers<TRes> {
  _CopyWithImpl$Variables$Query$GetRetailers(
    this._instance,
    this._then,
  );

  final Variables$Query$GetRetailers _instance;

  final TRes Function(Variables$Query$GetRetailers) _then;

  static const _undefined = <dynamic, dynamic>{};

  TRes call({
    Object? clientId = _undefined,
    Object? country = _undefined,
  }) =>
      _then(Variables$Query$GetRetailers._({
        ..._instance._$data,
        if (clientId != _undefined && clientId != null)
          'clientId': (clientId as String),
        if (country != _undefined && country != null)
          'country': (country as String),
      }));
}

class _CopyWithStubImpl$Variables$Query$GetRetailers<TRes>
    implements CopyWith$Variables$Query$GetRetailers<TRes> {
  _CopyWithStubImpl$Variables$Query$GetRetailers(this._res);

  TRes _res;

  call({
    String? clientId,
    String? country,
  }) =>
      _res;
}

class Query$GetRetailers {
  Query$GetRetailers({
    this.getRetailers,
    this.$__typename = 'Query',
  });

  factory Query$GetRetailers.fromJson(Map<String, dynamic> json) {
    final l$getRetailers = json['getRetailers'];
    final l$$__typename = json['__typename'];
    return Query$GetRetailers(
      getRetailers: (l$getRetailers as List<dynamic>?)
          ?.map((e) => e == null
              ? null
              : Query$GetRetailers$getRetailers.fromJson(
                  (e as Map<String, dynamic>)))
          .toList(),
      $__typename: (l$$__typename as String),
    );
  }

  final List<Query$GetRetailers$getRetailers?>? getRetailers;

  final String $__typename;

  Map<String, dynamic> toJson() {
    final _resultData = <String, dynamic>{};
    final l$getRetailers = getRetailers;
    _resultData['getRetailers'] =
        l$getRetailers?.map((e) => e?.toJson()).toList();
    final l$$__typename = $__typename;
    _resultData['__typename'] = l$$__typename;
    return _resultData;
  }

  @override
  int get hashCode {
    final l$getRetailers = getRetailers;
    final l$$__typename = $__typename;
    return Object.hashAll([
      l$getRetailers == null
          ? null
          : Object.hashAll(l$getRetailers.map((v) => v)),
      l$$__typename,
    ]);
  }

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) {
      return true;
    }
    if (!(other is Query$GetRetailers) || runtimeType != other.runtimeType) {
      return false;
    }
    final l$getRetailers = getRetailers;
    final lOther$getRetailers = other.getRetailers;
    if (l$getRetailers != null && lOther$getRetailers != null) {
      if (l$getRetailers.length != lOther$getRetailers.length) {
        return false;
      }
      for (int i = 0; i < l$getRetailers.length; i++) {
        final l$getRetailers$entry = l$getRetailers[i];
        final lOther$getRetailers$entry = lOther$getRetailers[i];
        if (l$getRetailers$entry != lOther$getRetailers$entry) {
          return false;
        }
      }
    } else if (l$getRetailers != lOther$getRetailers) {
      return false;
    }
    final l$$__typename = $__typename;
    final lOther$$__typename = other.$__typename;
    if (l$$__typename != lOther$$__typename) {
      return false;
    }
    return true;
  }
}

extension UtilityExtension$Query$GetRetailers on Query$GetRetailers {
  CopyWith$Query$GetRetailers<Query$GetRetailers> get copyWith =>
      CopyWith$Query$GetRetailers(
        this,
        (i) => i,
      );
}

abstract class CopyWith$Query$GetRetailers<TRes> {
  factory CopyWith$Query$GetRetailers(
    Query$GetRetailers instance,
    TRes Function(Query$GetRetailers) then,
  ) = _CopyWithImpl$Query$GetRetailers;

  factory CopyWith$Query$GetRetailers.stub(TRes res) =
      _CopyWithStubImpl$Query$GetRetailers;

  TRes call({
    List<Query$GetRetailers$getRetailers?>? getRetailers,
    String? $__typename,
  });
  TRes getRetailers(
      Iterable<Query$GetRetailers$getRetailers?>? Function(
              Iterable<
                  CopyWith$Query$GetRetailers$getRetailers<
                      Query$GetRetailers$getRetailers>?>?)
          _fn);
}

class _CopyWithImpl$Query$GetRetailers<TRes>
    implements CopyWith$Query$GetRetailers<TRes> {
  _CopyWithImpl$Query$GetRetailers(
    this._instance,
    this._then,
  );

  final Query$GetRetailers _instance;

  final TRes Function(Query$GetRetailers) _then;

  static const _undefined = <dynamic, dynamic>{};

  TRes call({
    Object? getRetailers = _undefined,
    Object? $__typename = _undefined,
  }) =>
      _then(Query$GetRetailers(
        getRetailers: getRetailers == _undefined
            ? _instance.getRetailers
            : (getRetailers as List<Query$GetRetailers$getRetailers?>?),
        $__typename: $__typename == _undefined || $__typename == null
            ? _instance.$__typename
            : ($__typename as String),
      ));

  TRes getRetailers(
          Iterable<Query$GetRetailers$getRetailers?>? Function(
                  Iterable<
                      CopyWith$Query$GetRetailers$getRetailers<
                          Query$GetRetailers$getRetailers>?>?)
              _fn) =>
      call(
          getRetailers: _fn(_instance.getRetailers?.map((e) => e == null
              ? null
              : CopyWith$Query$GetRetailers$getRetailers(
                  e,
                  (i) => i,
                )))?.toList());
}

class _CopyWithStubImpl$Query$GetRetailers<TRes>
    implements CopyWith$Query$GetRetailers<TRes> {
  _CopyWithStubImpl$Query$GetRetailers(this._res);

  TRes _res;

  call({
    List<Query$GetRetailers$getRetailers?>? getRetailers,
    String? $__typename,
  }) =>
      _res;

  getRetailers(_fn) => _res;
}

const documentNodeQueryGetRetailers = DocumentNode(definitions: [
  OperationDefinitionNode(
    type: OperationType.query,
    name: NameNode(value: 'GetRetailers'),
    variableDefinitions: [
      VariableDefinitionNode(
        variable: VariableNode(name: NameNode(value: 'clientId')),
        type: NamedTypeNode(
          name: NameNode(value: 'String'),
          isNonNull: true,
        ),
        defaultValue: DefaultValueNode(value: null),
        directives: [],
      ),
      VariableDefinitionNode(
        variable: VariableNode(name: NameNode(value: 'country')),
        type: NamedTypeNode(
          name: NameNode(value: 'String'),
          isNonNull: true,
        ),
        defaultValue: DefaultValueNode(value: null),
        directives: [],
      ),
    ],
    directives: [],
    selectionSet: SelectionSetNode(selections: [
      FieldNode(
        name: NameNode(value: 'getRetailers'),
        alias: null,
        arguments: [
          ArgumentNode(
            name: NameNode(value: 'clientId'),
            value: VariableNode(name: NameNode(value: 'clientId')),
          ),
          ArgumentNode(
            name: NameNode(value: 'country'),
            value: VariableNode(name: NameNode(value: 'country')),
          ),
        ],
        directives: [],
        selectionSet: SelectionSetNode(selections: [
          FieldNode(
            name: NameNode(value: 'commissions'),
            alias: null,
            arguments: [],
            directives: [],
            selectionSet: SelectionSetNode(selections: [
              FieldNode(
                name: NameNode(value: 'clientId'),
                alias: null,
                arguments: [],
                directives: [],
                selectionSet: null,
              ),
              FieldNode(
                name: NameNode(value: 'commissionId'),
                alias: null,
                arguments: [],
                directives: [],
                selectionSet: null,
              ),
              FieldNode(
                name: NameNode(value: 'retailerId'),
                alias: null,
                arguments: [],
                directives: [],
                selectionSet: null,
              ),
              FieldNode(
                name: NameNode(value: '__typename'),
                alias: null,
                arguments: [],
                directives: [],
                selectionSet: null,
              ),
            ]),
          ),
          FieldNode(
            name: NameNode(value: 'country'),
            alias: null,
            arguments: [],
            directives: [],
            selectionSet: null,
          ),
          FieldNode(
            name: NameNode(value: 'name'),
            alias: null,
            arguments: [],
            directives: [],
            selectionSet: null,
          ),
          FieldNode(
            name: NameNode(value: 'logo'),
            alias: null,
            arguments: [],
            directives: [],
            selectionSet: null,
          ),
          FieldNode(
            name: NameNode(value: '__typename'),
            alias: null,
            arguments: [],
            directives: [],
            selectionSet: null,
          ),
        ]),
      ),
      FieldNode(
        name: NameNode(value: '__typename'),
        alias: null,
        arguments: [],
        directives: [],
        selectionSet: null,
      ),
    ]),
  ),
]);
Query$GetRetailers _parserFn$Query$GetRetailers(Map<String, dynamic> data) =>
    Query$GetRetailers.fromJson(data);
typedef OnQueryComplete$Query$GetRetailers = FutureOr<void> Function(
  Map<String, dynamic>?,
  Query$GetRetailers?,
);

class Options$Query$GetRetailers
    extends graphql.QueryOptions<Query$GetRetailers> {
  Options$Query$GetRetailers({
    String? operationName,
    required Variables$Query$GetRetailers variables,
    graphql.FetchPolicy? fetchPolicy,
    graphql.ErrorPolicy? errorPolicy,
    graphql.CacheRereadPolicy? cacheRereadPolicy,
    Object? optimisticResult,
    Query$GetRetailers? typedOptimisticResult,
    Duration? pollInterval,
    graphql.Context? context,
    OnQueryComplete$Query$GetRetailers? onComplete,
    graphql.OnQueryError? onError,
  })  : onCompleteWithParsed = onComplete,
        super(
          variables: variables.toJson(),
          operationName: operationName,
          fetchPolicy: fetchPolicy,
          errorPolicy: errorPolicy,
          cacheRereadPolicy: cacheRereadPolicy,
          optimisticResult: optimisticResult ?? typedOptimisticResult?.toJson(),
          pollInterval: pollInterval,
          context: context,
          onComplete: onComplete == null
              ? null
              : (data) => onComplete(
                    data,
                    data == null ? null : _parserFn$Query$GetRetailers(data),
                  ),
          onError: onError,
          document: documentNodeQueryGetRetailers,
          parserFn: _parserFn$Query$GetRetailers,
        );

  final OnQueryComplete$Query$GetRetailers? onCompleteWithParsed;

  @override
  List<Object?> get properties => [
        ...super.onComplete == null
            ? super.properties
            : super.properties.where((property) => property != onComplete),
        onCompleteWithParsed,
      ];
}

class WatchOptions$Query$GetRetailers
    extends graphql.WatchQueryOptions<Query$GetRetailers> {
  WatchOptions$Query$GetRetailers({
    String? operationName,
    required Variables$Query$GetRetailers variables,
    graphql.FetchPolicy? fetchPolicy,
    graphql.ErrorPolicy? errorPolicy,
    graphql.CacheRereadPolicy? cacheRereadPolicy,
    Object? optimisticResult,
    Query$GetRetailers? typedOptimisticResult,
    graphql.Context? context,
    Duration? pollInterval,
    bool? eagerlyFetchResults,
    bool carryForwardDataOnException = true,
    bool fetchResults = false,
  }) : super(
          variables: variables.toJson(),
          operationName: operationName,
          fetchPolicy: fetchPolicy,
          errorPolicy: errorPolicy,
          cacheRereadPolicy: cacheRereadPolicy,
          optimisticResult: optimisticResult ?? typedOptimisticResult?.toJson(),
          context: context,
          document: documentNodeQueryGetRetailers,
          pollInterval: pollInterval,
          eagerlyFetchResults: eagerlyFetchResults,
          carryForwardDataOnException: carryForwardDataOnException,
          fetchResults: fetchResults,
          parserFn: _parserFn$Query$GetRetailers,
        );
}

class FetchMoreOptions$Query$GetRetailers extends graphql.FetchMoreOptions {
  FetchMoreOptions$Query$GetRetailers({
    required graphql.UpdateQuery updateQuery,
    required Variables$Query$GetRetailers variables,
  }) : super(
          updateQuery: updateQuery,
          variables: variables.toJson(),
          document: documentNodeQueryGetRetailers,
        );
}

extension ClientExtension$Query$GetRetailers on graphql.GraphQLClient {
  Future<graphql.QueryResult<Query$GetRetailers>> query$GetRetailers(
          Options$Query$GetRetailers options) async =>
      await this.query(options);
  graphql.ObservableQuery<Query$GetRetailers> watchQuery$GetRetailers(
          WatchOptions$Query$GetRetailers options) =>
      this.watchQuery(options);
  void writeQuery$GetRetailers({
    required Query$GetRetailers data,
    required Variables$Query$GetRetailers variables,
    bool broadcast = true,
  }) =>
      this.writeQuery(
        graphql.Request(
          operation: graphql.Operation(document: documentNodeQueryGetRetailers),
          variables: variables.toJson(),
        ),
        data: data.toJson(),
        broadcast: broadcast,
      );
  Query$GetRetailers? readQuery$GetRetailers({
    required Variables$Query$GetRetailers variables,
    bool optimistic = true,
  }) {
    final result = this.readQuery(
      graphql.Request(
        operation: graphql.Operation(document: documentNodeQueryGetRetailers),
        variables: variables.toJson(),
      ),
      optimistic: optimistic,
    );
    return result == null ? null : Query$GetRetailers.fromJson(result);
  }
}

graphql_flutter.QueryHookResult<Query$GetRetailers> useQuery$GetRetailers(
        Options$Query$GetRetailers options) =>
    graphql_flutter.useQuery(options);
graphql.ObservableQuery<Query$GetRetailers> useWatchQuery$GetRetailers(
        WatchOptions$Query$GetRetailers options) =>
    graphql_flutter.useWatchQuery(options);

class Query$GetRetailers$Widget
    extends graphql_flutter.Query<Query$GetRetailers> {
  Query$GetRetailers$Widget({
    widgets.Key? key,
    required Options$Query$GetRetailers options,
    required graphql_flutter.QueryBuilder<Query$GetRetailers> builder,
  }) : super(
          key: key,
          options: options,
          builder: builder,
        );
}

class Query$GetRetailers$getRetailers {
  Query$GetRetailers$getRetailers({
    required this.commissions,
    this.country,
    this.name,
    this.logo,
    this.$__typename = 'Retailer',
  });

  factory Query$GetRetailers$getRetailers.fromJson(Map<String, dynamic> json) {
    final l$commissions = json['commissions'];
    final l$country = json['country'];
    final l$name = json['name'];
    final l$logo = json['logo'];
    final l$$__typename = json['__typename'];
    return Query$GetRetailers$getRetailers(
      commissions: (l$commissions as List<dynamic>)
          .map((e) => Query$GetRetailers$getRetailers$commissions.fromJson(
              (e as Map<String, dynamic>)))
          .toList(),
      country: (l$country as String?),
      name: (l$name as String?),
      logo: (l$logo as String?),
      $__typename: (l$$__typename as String),
    );
  }

  final List<Query$GetRetailers$getRetailers$commissions> commissions;

  final String? country;

  final String? name;

  final String? logo;

  final String $__typename;

  Map<String, dynamic> toJson() {
    final _resultData = <String, dynamic>{};
    final l$commissions = commissions;
    _resultData['commissions'] = l$commissions.map((e) => e.toJson()).toList();
    final l$country = country;
    _resultData['country'] = l$country;
    final l$name = name;
    _resultData['name'] = l$name;
    final l$logo = logo;
    _resultData['logo'] = l$logo;
    final l$$__typename = $__typename;
    _resultData['__typename'] = l$$__typename;
    return _resultData;
  }

  @override
  int get hashCode {
    final l$commissions = commissions;
    final l$country = country;
    final l$name = name;
    final l$logo = logo;
    final l$$__typename = $__typename;
    return Object.hashAll([
      Object.hashAll(l$commissions.map((v) => v)),
      l$country,
      l$name,
      l$logo,
      l$$__typename,
    ]);
  }

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) {
      return true;
    }
    if (!(other is Query$GetRetailers$getRetailers) ||
        runtimeType != other.runtimeType) {
      return false;
    }
    final l$commissions = commissions;
    final lOther$commissions = other.commissions;
    if (l$commissions.length != lOther$commissions.length) {
      return false;
    }
    for (int i = 0; i < l$commissions.length; i++) {
      final l$commissions$entry = l$commissions[i];
      final lOther$commissions$entry = lOther$commissions[i];
      if (l$commissions$entry != lOther$commissions$entry) {
        return false;
      }
    }
    final l$country = country;
    final lOther$country = other.country;
    if (l$country != lOther$country) {
      return false;
    }
    final l$name = name;
    final lOther$name = other.name;
    if (l$name != lOther$name) {
      return false;
    }
    final l$logo = logo;
    final lOther$logo = other.logo;
    if (l$logo != lOther$logo) {
      return false;
    }
    final l$$__typename = $__typename;
    final lOther$$__typename = other.$__typename;
    if (l$$__typename != lOther$$__typename) {
      return false;
    }
    return true;
  }
}

extension UtilityExtension$Query$GetRetailers$getRetailers
    on Query$GetRetailers$getRetailers {
  CopyWith$Query$GetRetailers$getRetailers<Query$GetRetailers$getRetailers>
      get copyWith => CopyWith$Query$GetRetailers$getRetailers(
            this,
            (i) => i,
          );
}

abstract class CopyWith$Query$GetRetailers$getRetailers<TRes> {
  factory CopyWith$Query$GetRetailers$getRetailers(
    Query$GetRetailers$getRetailers instance,
    TRes Function(Query$GetRetailers$getRetailers) then,
  ) = _CopyWithImpl$Query$GetRetailers$getRetailers;

  factory CopyWith$Query$GetRetailers$getRetailers.stub(TRes res) =
      _CopyWithStubImpl$Query$GetRetailers$getRetailers;

  TRes call({
    List<Query$GetRetailers$getRetailers$commissions>? commissions,
    String? country,
    String? name,
    String? logo,
    String? $__typename,
  });
  TRes commissions(
      Iterable<Query$GetRetailers$getRetailers$commissions> Function(
              Iterable<
                  CopyWith$Query$GetRetailers$getRetailers$commissions<
                      Query$GetRetailers$getRetailers$commissions>>)
          _fn);
}

class _CopyWithImpl$Query$GetRetailers$getRetailers<TRes>
    implements CopyWith$Query$GetRetailers$getRetailers<TRes> {
  _CopyWithImpl$Query$GetRetailers$getRetailers(
    this._instance,
    this._then,
  );

  final Query$GetRetailers$getRetailers _instance;

  final TRes Function(Query$GetRetailers$getRetailers) _then;

  static const _undefined = <dynamic, dynamic>{};

  TRes call({
    Object? commissions = _undefined,
    Object? country = _undefined,
    Object? name = _undefined,
    Object? logo = _undefined,
    Object? $__typename = _undefined,
  }) =>
      _then(Query$GetRetailers$getRetailers(
        commissions: commissions == _undefined || commissions == null
            ? _instance.commissions
            : (commissions
                as List<Query$GetRetailers$getRetailers$commissions>),
        country:
            country == _undefined ? _instance.country : (country as String?),
        name: name == _undefined ? _instance.name : (name as String?),
        logo: logo == _undefined ? _instance.logo : (logo as String?),
        $__typename: $__typename == _undefined || $__typename == null
            ? _instance.$__typename
            : ($__typename as String),
      ));

  TRes commissions(
          Iterable<Query$GetRetailers$getRetailers$commissions> Function(
                  Iterable<
                      CopyWith$Query$GetRetailers$getRetailers$commissions<
                          Query$GetRetailers$getRetailers$commissions>>)
              _fn) =>
      call(
          commissions: _fn(_instance.commissions
              .map((e) => CopyWith$Query$GetRetailers$getRetailers$commissions(
                    e,
                    (i) => i,
                  ))).toList());
}

class _CopyWithStubImpl$Query$GetRetailers$getRetailers<TRes>
    implements CopyWith$Query$GetRetailers$getRetailers<TRes> {
  _CopyWithStubImpl$Query$GetRetailers$getRetailers(this._res);

  TRes _res;

  call({
    List<Query$GetRetailers$getRetailers$commissions>? commissions,
    String? country,
    String? name,
    String? logo,
    String? $__typename,
  }) =>
      _res;

  commissions(_fn) => _res;
}

class Query$GetRetailers$getRetailers$commissions {
  Query$GetRetailers$getRetailers$commissions({
    this.clientId,
    this.commissionId,
    this.retailerId,
    this.$__typename = 'Commission',
  });

  factory Query$GetRetailers$getRetailers$commissions.fromJson(
      Map<String, dynamic> json) {
    final l$clientId = json['clientId'];
    final l$commissionId = json['commissionId'];
    final l$retailerId = json['retailerId'];
    final l$$__typename = json['__typename'];
    return Query$GetRetailers$getRetailers$commissions(
      clientId: (l$clientId as String?),
      commissionId: (l$commissionId as String?),
      retailerId: (l$retailerId as String?),
      $__typename: (l$$__typename as String),
    );
  }

  final String? clientId;

  final String? commissionId;

  final String? retailerId;

  final String $__typename;

  Map<String, dynamic> toJson() {
    final _resultData = <String, dynamic>{};
    final l$clientId = clientId;
    _resultData['clientId'] = l$clientId;
    final l$commissionId = commissionId;
    _resultData['commissionId'] = l$commissionId;
    final l$retailerId = retailerId;
    _resultData['retailerId'] = l$retailerId;
    final l$$__typename = $__typename;
    _resultData['__typename'] = l$$__typename;
    return _resultData;
  }

  @override
  int get hashCode {
    final l$clientId = clientId;
    final l$commissionId = commissionId;
    final l$retailerId = retailerId;
    final l$$__typename = $__typename;
    return Object.hashAll([
      l$clientId,
      l$commissionId,
      l$retailerId,
      l$$__typename,
    ]);
  }

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) {
      return true;
    }
    if (!(other is Query$GetRetailers$getRetailers$commissions) ||
        runtimeType != other.runtimeType) {
      return false;
    }
    final l$clientId = clientId;
    final lOther$clientId = other.clientId;
    if (l$clientId != lOther$clientId) {
      return false;
    }
    final l$commissionId = commissionId;
    final lOther$commissionId = other.commissionId;
    if (l$commissionId != lOther$commissionId) {
      return false;
    }
    final l$retailerId = retailerId;
    final lOther$retailerId = other.retailerId;
    if (l$retailerId != lOther$retailerId) {
      return false;
    }
    final l$$__typename = $__typename;
    final lOther$$__typename = other.$__typename;
    if (l$$__typename != lOther$$__typename) {
      return false;
    }
    return true;
  }
}

extension UtilityExtension$Query$GetRetailers$getRetailers$commissions
    on Query$GetRetailers$getRetailers$commissions {
  CopyWith$Query$GetRetailers$getRetailers$commissions<
          Query$GetRetailers$getRetailers$commissions>
      get copyWith => CopyWith$Query$GetRetailers$getRetailers$commissions(
            this,
            (i) => i,
          );
}

abstract class CopyWith$Query$GetRetailers$getRetailers$commissions<TRes> {
  factory CopyWith$Query$GetRetailers$getRetailers$commissions(
    Query$GetRetailers$getRetailers$commissions instance,
    TRes Function(Query$GetRetailers$getRetailers$commissions) then,
  ) = _CopyWithImpl$Query$GetRetailers$getRetailers$commissions;

  factory CopyWith$Query$GetRetailers$getRetailers$commissions.stub(TRes res) =
      _CopyWithStubImpl$Query$GetRetailers$getRetailers$commissions;

  TRes call({
    String? clientId,
    String? commissionId,
    String? retailerId,
    String? $__typename,
  });
}

class _CopyWithImpl$Query$GetRetailers$getRetailers$commissions<TRes>
    implements CopyWith$Query$GetRetailers$getRetailers$commissions<TRes> {
  _CopyWithImpl$Query$GetRetailers$getRetailers$commissions(
    this._instance,
    this._then,
  );

  final Query$GetRetailers$getRetailers$commissions _instance;

  final TRes Function(Query$GetRetailers$getRetailers$commissions) _then;

  static const _undefined = <dynamic, dynamic>{};

  TRes call({
    Object? clientId = _undefined,
    Object? commissionId = _undefined,
    Object? retailerId = _undefined,
    Object? $__typename = _undefined,
  }) =>
      _then(Query$GetRetailers$getRetailers$commissions(
        clientId:
            clientId == _undefined ? _instance.clientId : (clientId as String?),
        commissionId: commissionId == _undefined
            ? _instance.commissionId
            : (commissionId as String?),
        retailerId: retailerId == _undefined
            ? _instance.retailerId
            : (retailerId as String?),
        $__typename: $__typename == _undefined || $__typename == null
            ? _instance.$__typename
            : ($__typename as String),
      ));
}

class _CopyWithStubImpl$Query$GetRetailers$getRetailers$commissions<TRes>
    implements CopyWith$Query$GetRetailers$getRetailers$commissions<TRes> {
  _CopyWithStubImpl$Query$GetRetailers$getRetailers$commissions(this._res);

  TRes _res;

  call({
    String? clientId,
    String? commissionId,
    String? retailerId,
    String? $__typename,
  }) =>
      _res;
}
