class Input$CreateCommissionDto {
  factory Input$CreateCommissionDto({
    required String clientId,
    required String retailerId,
    required Input$CreateIncomingCommissionDto incoming,
    required Input$CreateOutgoingCommissionDto outgoing,
    required String startDate,
    String? endDate,
    required String description,
    required String warning,
    String? parentCommissionId,
  }) =>
      Input$CreateCommissionDto._({
        r'clientId': clientId,
        r'retailerId': retailerId,
        r'incoming': incoming,
        r'outgoing': outgoing,
        r'startDate': startDate,
        if (endDate != null) r'endDate': endDate,
        r'description': description,
        r'warning': warning,
        if (parentCommissionId != null)
          r'parentCommissionId': parentCommissionId,
      });

  Input$CreateCommissionDto._(this._$data);

  factory Input$CreateCommissionDto.fromJson(Map<String, dynamic> data) {
    final result$data = <String, dynamic>{};
    final l$clientId = data['clientId'];
    result$data['clientId'] = (l$clientId as String);
    final l$retailerId = data['retailerId'];
    result$data['retailerId'] = (l$retailerId as String);
    final l$incoming = data['incoming'];
    result$data['incoming'] = Input$CreateIncomingCommissionDto.fromJson(
        (l$incoming as Map<String, dynamic>));
    final l$outgoing = data['outgoing'];
    result$data['outgoing'] = Input$CreateOutgoingCommissionDto.fromJson(
        (l$outgoing as Map<String, dynamic>));
    final l$startDate = data['startDate'];
    result$data['startDate'] = (l$startDate as String);
    if (data.containsKey('endDate')) {
      final l$endDate = data['endDate'];
      result$data['endDate'] = (l$endDate as String?);
    }
    final l$description = data['description'];
    result$data['description'] = (l$description as String);
    final l$warning = data['warning'];
    result$data['warning'] = (l$warning as String);
    if (data.containsKey('parentCommissionId')) {
      final l$parentCommissionId = data['parentCommissionId'];
      result$data['parentCommissionId'] = (l$parentCommissionId as String?);
    }
    return Input$CreateCommissionDto._(result$data);
  }

  Map<String, dynamic> _$data;

  String get clientId => (_$data['clientId'] as String);

  String get retailerId => (_$data['retailerId'] as String);

  Input$CreateIncomingCommissionDto get incoming =>
      (_$data['incoming'] as Input$CreateIncomingCommissionDto);

  Input$CreateOutgoingCommissionDto get outgoing =>
      (_$data['outgoing'] as Input$CreateOutgoingCommissionDto);

  String get startDate => (_$data['startDate'] as String);

  String? get endDate => (_$data['endDate'] as String?);

  String get description => (_$data['description'] as String);

  String get warning => (_$data['warning'] as String);

  String? get parentCommissionId => (_$data['parentCommissionId'] as String?);

  Map<String, dynamic> toJson() {
    final result$data = <String, dynamic>{};
    final l$clientId = clientId;
    result$data['clientId'] = l$clientId;
    final l$retailerId = retailerId;
    result$data['retailerId'] = l$retailerId;
    final l$incoming = incoming;
    result$data['incoming'] = l$incoming.toJson();
    final l$outgoing = outgoing;
    result$data['outgoing'] = l$outgoing.toJson();
    final l$startDate = startDate;
    result$data['startDate'] = l$startDate;
    if (_$data.containsKey('endDate')) {
      final l$endDate = endDate;
      result$data['endDate'] = l$endDate;
    }
    final l$description = description;
    result$data['description'] = l$description;
    final l$warning = warning;
    result$data['warning'] = l$warning;
    if (_$data.containsKey('parentCommissionId')) {
      final l$parentCommissionId = parentCommissionId;
      result$data['parentCommissionId'] = l$parentCommissionId;
    }
    return result$data;
  }

  CopyWith$Input$CreateCommissionDto<Input$CreateCommissionDto> get copyWith =>
      CopyWith$Input$CreateCommissionDto(
        this,
        (i) => i,
      );

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) {
      return true;
    }
    if (!(other is Input$CreateCommissionDto) ||
        runtimeType != other.runtimeType) {
      return false;
    }
    final l$clientId = clientId;
    final lOther$clientId = other.clientId;
    if (l$clientId != lOther$clientId) {
      return false;
    }
    final l$retailerId = retailerId;
    final lOther$retailerId = other.retailerId;
    if (l$retailerId != lOther$retailerId) {
      return false;
    }
    final l$incoming = incoming;
    final lOther$incoming = other.incoming;
    if (l$incoming != lOther$incoming) {
      return false;
    }
    final l$outgoing = outgoing;
    final lOther$outgoing = other.outgoing;
    if (l$outgoing != lOther$outgoing) {
      return false;
    }
    final l$startDate = startDate;
    final lOther$startDate = other.startDate;
    if (l$startDate != lOther$startDate) {
      return false;
    }
    final l$endDate = endDate;
    final lOther$endDate = other.endDate;
    if (_$data.containsKey('endDate') != other._$data.containsKey('endDate')) {
      return false;
    }
    if (l$endDate != lOther$endDate) {
      return false;
    }
    final l$description = description;
    final lOther$description = other.description;
    if (l$description != lOther$description) {
      return false;
    }
    final l$warning = warning;
    final lOther$warning = other.warning;
    if (l$warning != lOther$warning) {
      return false;
    }
    final l$parentCommissionId = parentCommissionId;
    final lOther$parentCommissionId = other.parentCommissionId;
    if (_$data.containsKey('parentCommissionId') !=
        other._$data.containsKey('parentCommissionId')) {
      return false;
    }
    if (l$parentCommissionId != lOther$parentCommissionId) {
      return false;
    }
    return true;
  }

  @override
  int get hashCode {
    final l$clientId = clientId;
    final l$retailerId = retailerId;
    final l$incoming = incoming;
    final l$outgoing = outgoing;
    final l$startDate = startDate;
    final l$endDate = endDate;
    final l$description = description;
    final l$warning = warning;
    final l$parentCommissionId = parentCommissionId;
    return Object.hashAll([
      l$clientId,
      l$retailerId,
      l$incoming,
      l$outgoing,
      l$startDate,
      _$data.containsKey('endDate') ? l$endDate : const {},
      l$description,
      l$warning,
      _$data.containsKey('parentCommissionId')
          ? l$parentCommissionId
          : const {},
    ]);
  }
}

abstract class CopyWith$Input$CreateCommissionDto<TRes> {
  factory CopyWith$Input$CreateCommissionDto(
    Input$CreateCommissionDto instance,
    TRes Function(Input$CreateCommissionDto) then,
  ) = _CopyWithImpl$Input$CreateCommissionDto;

  factory CopyWith$Input$CreateCommissionDto.stub(TRes res) =
      _CopyWithStubImpl$Input$CreateCommissionDto;

  TRes call({
    String? clientId,
    String? retailerId,
    Input$CreateIncomingCommissionDto? incoming,
    Input$CreateOutgoingCommissionDto? outgoing,
    String? startDate,
    String? endDate,
    String? description,
    String? warning,
    String? parentCommissionId,
  });
  CopyWith$Input$CreateIncomingCommissionDto<TRes> get incoming;
  CopyWith$Input$CreateOutgoingCommissionDto<TRes> get outgoing;
}

class _CopyWithImpl$Input$CreateCommissionDto<TRes>
    implements CopyWith$Input$CreateCommissionDto<TRes> {
  _CopyWithImpl$Input$CreateCommissionDto(
    this._instance,
    this._then,
  );

  final Input$CreateCommissionDto _instance;

  final TRes Function(Input$CreateCommissionDto) _then;

  static const _undefined = <dynamic, dynamic>{};

  TRes call({
    Object? clientId = _undefined,
    Object? retailerId = _undefined,
    Object? incoming = _undefined,
    Object? outgoing = _undefined,
    Object? startDate = _undefined,
    Object? endDate = _undefined,
    Object? description = _undefined,
    Object? warning = _undefined,
    Object? parentCommissionId = _undefined,
  }) =>
      _then(Input$CreateCommissionDto._({
        ..._instance._$data,
        if (clientId != _undefined && clientId != null)
          'clientId': (clientId as String),
        if (retailerId != _undefined && retailerId != null)
          'retailerId': (retailerId as String),
        if (incoming != _undefined && incoming != null)
          'incoming': (incoming as Input$CreateIncomingCommissionDto),
        if (outgoing != _undefined && outgoing != null)
          'outgoing': (outgoing as Input$CreateOutgoingCommissionDto),
        if (startDate != _undefined && startDate != null)
          'startDate': (startDate as String),
        if (endDate != _undefined) 'endDate': (endDate as String?),
        if (description != _undefined && description != null)
          'description': (description as String),
        if (warning != _undefined && warning != null)
          'warning': (warning as String),
        if (parentCommissionId != _undefined)
          'parentCommissionId': (parentCommissionId as String?),
      }));

  CopyWith$Input$CreateIncomingCommissionDto<TRes> get incoming {
    final local$incoming = _instance.incoming;
    return CopyWith$Input$CreateIncomingCommissionDto(
        local$incoming, (e) => call(incoming: e));
  }

  CopyWith$Input$CreateOutgoingCommissionDto<TRes> get outgoing {
    final local$outgoing = _instance.outgoing;
    return CopyWith$Input$CreateOutgoingCommissionDto(
        local$outgoing, (e) => call(outgoing: e));
  }
}

class _CopyWithStubImpl$Input$CreateCommissionDto<TRes>
    implements CopyWith$Input$CreateCommissionDto<TRes> {
  _CopyWithStubImpl$Input$CreateCommissionDto(this._res);

  TRes _res;

  call({
    String? clientId,
    String? retailerId,
    Input$CreateIncomingCommissionDto? incoming,
    Input$CreateOutgoingCommissionDto? outgoing,
    String? startDate,
    String? endDate,
    String? description,
    String? warning,
    String? parentCommissionId,
  }) =>
      _res;

  CopyWith$Input$CreateIncomingCommissionDto<TRes> get incoming =>
      CopyWith$Input$CreateIncomingCommissionDto.stub(_res);

  CopyWith$Input$CreateOutgoingCommissionDto<TRes> get outgoing =>
      CopyWith$Input$CreateOutgoingCommissionDto.stub(_res);
}

class Input$CreateIncomingCommissionDto {
  factory Input$CreateIncomingCommissionDto({
    required Enum$CommissionType type,
    required double from,
    double? to,
  }) =>
      Input$CreateIncomingCommissionDto._({
        r'type': type,
        r'from': from,
        if (to != null) r'to': to,
      });

  Input$CreateIncomingCommissionDto._(this._$data);

  factory Input$CreateIncomingCommissionDto.fromJson(
      Map<String, dynamic> data) {
    final result$data = <String, dynamic>{};
    final l$type = data['type'];
    result$data['type'] = fromJson$Enum$CommissionType((l$type as String));
    final l$from = data['from'];
    result$data['from'] = (l$from as num).toDouble();
    if (data.containsKey('to')) {
      final l$to = data['to'];
      result$data['to'] = (l$to as num?)?.toDouble();
    }
    return Input$CreateIncomingCommissionDto._(result$data);
  }

  Map<String, dynamic> _$data;

  Enum$CommissionType get type => (_$data['type'] as Enum$CommissionType);

  double get from => (_$data['from'] as double);

  double? get to => (_$data['to'] as double?);

  Map<String, dynamic> toJson() {
    final result$data = <String, dynamic>{};
    final l$type = type;
    result$data['type'] = toJson$Enum$CommissionType(l$type);
    final l$from = from;
    result$data['from'] = l$from;
    if (_$data.containsKey('to')) {
      final l$to = to;
      result$data['to'] = l$to;
    }
    return result$data;
  }

  CopyWith$Input$CreateIncomingCommissionDto<Input$CreateIncomingCommissionDto>
      get copyWith => CopyWith$Input$CreateIncomingCommissionDto(
            this,
            (i) => i,
          );

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) {
      return true;
    }
    if (!(other is Input$CreateIncomingCommissionDto) ||
        runtimeType != other.runtimeType) {
      return false;
    }
    final l$type = type;
    final lOther$type = other.type;
    if (l$type != lOther$type) {
      return false;
    }
    final l$from = from;
    final lOther$from = other.from;
    if (l$from != lOther$from) {
      return false;
    }
    final l$to = to;
    final lOther$to = other.to;
    if (_$data.containsKey('to') != other._$data.containsKey('to')) {
      return false;
    }
    if (l$to != lOther$to) {
      return false;
    }
    return true;
  }

  @override
  int get hashCode {
    final l$type = type;
    final l$from = from;
    final l$to = to;
    return Object.hashAll([
      l$type,
      l$from,
      _$data.containsKey('to') ? l$to : const {},
    ]);
  }
}

abstract class CopyWith$Input$CreateIncomingCommissionDto<TRes> {
  factory CopyWith$Input$CreateIncomingCommissionDto(
    Input$CreateIncomingCommissionDto instance,
    TRes Function(Input$CreateIncomingCommissionDto) then,
  ) = _CopyWithImpl$Input$CreateIncomingCommissionDto;

  factory CopyWith$Input$CreateIncomingCommissionDto.stub(TRes res) =
      _CopyWithStubImpl$Input$CreateIncomingCommissionDto;

  TRes call({
    Enum$CommissionType? type,
    double? from,
    double? to,
  });
}

class _CopyWithImpl$Input$CreateIncomingCommissionDto<TRes>
    implements CopyWith$Input$CreateIncomingCommissionDto<TRes> {
  _CopyWithImpl$Input$CreateIncomingCommissionDto(
    this._instance,
    this._then,
  );

  final Input$CreateIncomingCommissionDto _instance;

  final TRes Function(Input$CreateIncomingCommissionDto) _then;

  static const _undefined = <dynamic, dynamic>{};

  TRes call({
    Object? type = _undefined,
    Object? from = _undefined,
    Object? to = _undefined,
  }) =>
      _then(Input$CreateIncomingCommissionDto._({
        ..._instance._$data,
        if (type != _undefined && type != null)
          'type': (type as Enum$CommissionType),
        if (from != _undefined && from != null) 'from': (from as double),
        if (to != _undefined) 'to': (to as double?),
      }));
}

class _CopyWithStubImpl$Input$CreateIncomingCommissionDto<TRes>
    implements CopyWith$Input$CreateIncomingCommissionDto<TRes> {
  _CopyWithStubImpl$Input$CreateIncomingCommissionDto(this._res);

  TRes _res;

  call({
    Enum$CommissionType? type,
    double? from,
    double? to,
  }) =>
      _res;
}

class Input$CreateOutgoingCommissionDto {
  factory Input$CreateOutgoingCommissionDto({
    required Enum$CommissionType type,
    required double from,
  }) =>
      Input$CreateOutgoingCommissionDto._({
        r'type': type,
        r'from': from,
      });

  Input$CreateOutgoingCommissionDto._(this._$data);

  factory Input$CreateOutgoingCommissionDto.fromJson(
      Map<String, dynamic> data) {
    final result$data = <String, dynamic>{};
    final l$type = data['type'];
    result$data['type'] = fromJson$Enum$CommissionType((l$type as String));
    final l$from = data['from'];
    result$data['from'] = (l$from as num).toDouble();
    return Input$CreateOutgoingCommissionDto._(result$data);
  }

  Map<String, dynamic> _$data;

  Enum$CommissionType get type => (_$data['type'] as Enum$CommissionType);

  double get from => (_$data['from'] as double);

  Map<String, dynamic> toJson() {
    final result$data = <String, dynamic>{};
    final l$type = type;
    result$data['type'] = toJson$Enum$CommissionType(l$type);
    final l$from = from;
    result$data['from'] = l$from;
    return result$data;
  }

  CopyWith$Input$CreateOutgoingCommissionDto<Input$CreateOutgoingCommissionDto>
      get copyWith => CopyWith$Input$CreateOutgoingCommissionDto(
            this,
            (i) => i,
          );

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) {
      return true;
    }
    if (!(other is Input$CreateOutgoingCommissionDto) ||
        runtimeType != other.runtimeType) {
      return false;
    }
    final l$type = type;
    final lOther$type = other.type;
    if (l$type != lOther$type) {
      return false;
    }
    final l$from = from;
    final lOther$from = other.from;
    if (l$from != lOther$from) {
      return false;
    }
    return true;
  }

  @override
  int get hashCode {
    final l$type = type;
    final l$from = from;
    return Object.hashAll([
      l$type,
      l$from,
    ]);
  }
}

abstract class CopyWith$Input$CreateOutgoingCommissionDto<TRes> {
  factory CopyWith$Input$CreateOutgoingCommissionDto(
    Input$CreateOutgoingCommissionDto instance,
    TRes Function(Input$CreateOutgoingCommissionDto) then,
  ) = _CopyWithImpl$Input$CreateOutgoingCommissionDto;

  factory CopyWith$Input$CreateOutgoingCommissionDto.stub(TRes res) =
      _CopyWithStubImpl$Input$CreateOutgoingCommissionDto;

  TRes call({
    Enum$CommissionType? type,
    double? from,
  });
}

class _CopyWithImpl$Input$CreateOutgoingCommissionDto<TRes>
    implements CopyWith$Input$CreateOutgoingCommissionDto<TRes> {
  _CopyWithImpl$Input$CreateOutgoingCommissionDto(
    this._instance,
    this._then,
  );

  final Input$CreateOutgoingCommissionDto _instance;

  final TRes Function(Input$CreateOutgoingCommissionDto) _then;

  static const _undefined = <dynamic, dynamic>{};

  TRes call({
    Object? type = _undefined,
    Object? from = _undefined,
  }) =>
      _then(Input$CreateOutgoingCommissionDto._({
        ..._instance._$data,
        if (type != _undefined && type != null)
          'type': (type as Enum$CommissionType),
        if (from != _undefined && from != null) 'from': (from as double),
      }));
}

class _CopyWithStubImpl$Input$CreateOutgoingCommissionDto<TRes>
    implements CopyWith$Input$CreateOutgoingCommissionDto<TRes> {
  _CopyWithStubImpl$Input$CreateOutgoingCommissionDto(this._res);

  TRes _res;

  call({
    Enum$CommissionType? type,
    double? from,
  }) =>
      _res;
}

class Input$UpdateCommissionDto {
  factory Input$UpdateCommissionDto({
    required String commissionId,
    required String clientId,
    String? retailerId,
    Input$UpdateIncomingCommissionDto? incoming,
    Input$UpdateOutgoingCommissionDto? outgoing,
    String? startDate,
    String? endDate,
    String? description,
    String? warning,
  }) =>
      Input$UpdateCommissionDto._({
        r'commissionId': commissionId,
        r'clientId': clientId,
        if (retailerId != null) r'retailerId': retailerId,
        if (incoming != null) r'incoming': incoming,
        if (outgoing != null) r'outgoing': outgoing,
        if (startDate != null) r'startDate': startDate,
        if (endDate != null) r'endDate': endDate,
        if (description != null) r'description': description,
        if (warning != null) r'warning': warning,
      });

  Input$UpdateCommissionDto._(this._$data);

  factory Input$UpdateCommissionDto.fromJson(Map<String, dynamic> data) {
    final result$data = <String, dynamic>{};
    final l$commissionId = data['commissionId'];
    result$data['commissionId'] = (l$commissionId as String);
    final l$clientId = data['clientId'];
    result$data['clientId'] = (l$clientId as String);
    if (data.containsKey('retailerId')) {
      final l$retailerId = data['retailerId'];
      result$data['retailerId'] = (l$retailerId as String?);
    }
    if (data.containsKey('incoming')) {
      final l$incoming = data['incoming'];
      result$data['incoming'] = l$incoming == null
          ? null
          : Input$UpdateIncomingCommissionDto.fromJson(
              (l$incoming as Map<String, dynamic>));
    }
    if (data.containsKey('outgoing')) {
      final l$outgoing = data['outgoing'];
      result$data['outgoing'] = l$outgoing == null
          ? null
          : Input$UpdateOutgoingCommissionDto.fromJson(
              (l$outgoing as Map<String, dynamic>));
    }
    if (data.containsKey('startDate')) {
      final l$startDate = data['startDate'];
      result$data['startDate'] = (l$startDate as String?);
    }
    if (data.containsKey('endDate')) {
      final l$endDate = data['endDate'];
      result$data['endDate'] = (l$endDate as String?);
    }
    if (data.containsKey('description')) {
      final l$description = data['description'];
      result$data['description'] = (l$description as String?);
    }
    if (data.containsKey('warning')) {
      final l$warning = data['warning'];
      result$data['warning'] = (l$warning as String?);
    }
    return Input$UpdateCommissionDto._(result$data);
  }

  Map<String, dynamic> _$data;

  String get commissionId => (_$data['commissionId'] as String);

  String get clientId => (_$data['clientId'] as String);

  String? get retailerId => (_$data['retailerId'] as String?);

  Input$UpdateIncomingCommissionDto? get incoming =>
      (_$data['incoming'] as Input$UpdateIncomingCommissionDto?);

  Input$UpdateOutgoingCommissionDto? get outgoing =>
      (_$data['outgoing'] as Input$UpdateOutgoingCommissionDto?);

  String? get startDate => (_$data['startDate'] as String?);

  String? get endDate => (_$data['endDate'] as String?);

  String? get description => (_$data['description'] as String?);

  String? get warning => (_$data['warning'] as String?);

  Map<String, dynamic> toJson() {
    final result$data = <String, dynamic>{};
    final l$commissionId = commissionId;
    result$data['commissionId'] = l$commissionId;
    final l$clientId = clientId;
    result$data['clientId'] = l$clientId;
    if (_$data.containsKey('retailerId')) {
      final l$retailerId = retailerId;
      result$data['retailerId'] = l$retailerId;
    }
    if (_$data.containsKey('incoming')) {
      final l$incoming = incoming;
      result$data['incoming'] = l$incoming?.toJson();
    }
    if (_$data.containsKey('outgoing')) {
      final l$outgoing = outgoing;
      result$data['outgoing'] = l$outgoing?.toJson();
    }
    if (_$data.containsKey('startDate')) {
      final l$startDate = startDate;
      result$data['startDate'] = l$startDate;
    }
    if (_$data.containsKey('endDate')) {
      final l$endDate = endDate;
      result$data['endDate'] = l$endDate;
    }
    if (_$data.containsKey('description')) {
      final l$description = description;
      result$data['description'] = l$description;
    }
    if (_$data.containsKey('warning')) {
      final l$warning = warning;
      result$data['warning'] = l$warning;
    }
    return result$data;
  }

  CopyWith$Input$UpdateCommissionDto<Input$UpdateCommissionDto> get copyWith =>
      CopyWith$Input$UpdateCommissionDto(
        this,
        (i) => i,
      );

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) {
      return true;
    }
    if (!(other is Input$UpdateCommissionDto) ||
        runtimeType != other.runtimeType) {
      return false;
    }
    final l$commissionId = commissionId;
    final lOther$commissionId = other.commissionId;
    if (l$commissionId != lOther$commissionId) {
      return false;
    }
    final l$clientId = clientId;
    final lOther$clientId = other.clientId;
    if (l$clientId != lOther$clientId) {
      return false;
    }
    final l$retailerId = retailerId;
    final lOther$retailerId = other.retailerId;
    if (_$data.containsKey('retailerId') !=
        other._$data.containsKey('retailerId')) {
      return false;
    }
    if (l$retailerId != lOther$retailerId) {
      return false;
    }
    final l$incoming = incoming;
    final lOther$incoming = other.incoming;
    if (_$data.containsKey('incoming') !=
        other._$data.containsKey('incoming')) {
      return false;
    }
    if (l$incoming != lOther$incoming) {
      return false;
    }
    final l$outgoing = outgoing;
    final lOther$outgoing = other.outgoing;
    if (_$data.containsKey('outgoing') !=
        other._$data.containsKey('outgoing')) {
      return false;
    }
    if (l$outgoing != lOther$outgoing) {
      return false;
    }
    final l$startDate = startDate;
    final lOther$startDate = other.startDate;
    if (_$data.containsKey('startDate') !=
        other._$data.containsKey('startDate')) {
      return false;
    }
    if (l$startDate != lOther$startDate) {
      return false;
    }
    final l$endDate = endDate;
    final lOther$endDate = other.endDate;
    if (_$data.containsKey('endDate') != other._$data.containsKey('endDate')) {
      return false;
    }
    if (l$endDate != lOther$endDate) {
      return false;
    }
    final l$description = description;
    final lOther$description = other.description;
    if (_$data.containsKey('description') !=
        other._$data.containsKey('description')) {
      return false;
    }
    if (l$description != lOther$description) {
      return false;
    }
    final l$warning = warning;
    final lOther$warning = other.warning;
    if (_$data.containsKey('warning') != other._$data.containsKey('warning')) {
      return false;
    }
    if (l$warning != lOther$warning) {
      return false;
    }
    return true;
  }

  @override
  int get hashCode {
    final l$commissionId = commissionId;
    final l$clientId = clientId;
    final l$retailerId = retailerId;
    final l$incoming = incoming;
    final l$outgoing = outgoing;
    final l$startDate = startDate;
    final l$endDate = endDate;
    final l$description = description;
    final l$warning = warning;
    return Object.hashAll([
      l$commissionId,
      l$clientId,
      _$data.containsKey('retailerId') ? l$retailerId : const {},
      _$data.containsKey('incoming') ? l$incoming : const {},
      _$data.containsKey('outgoing') ? l$outgoing : const {},
      _$data.containsKey('startDate') ? l$startDate : const {},
      _$data.containsKey('endDate') ? l$endDate : const {},
      _$data.containsKey('description') ? l$description : const {},
      _$data.containsKey('warning') ? l$warning : const {},
    ]);
  }
}

abstract class CopyWith$Input$UpdateCommissionDto<TRes> {
  factory CopyWith$Input$UpdateCommissionDto(
    Input$UpdateCommissionDto instance,
    TRes Function(Input$UpdateCommissionDto) then,
  ) = _CopyWithImpl$Input$UpdateCommissionDto;

  factory CopyWith$Input$UpdateCommissionDto.stub(TRes res) =
      _CopyWithStubImpl$Input$UpdateCommissionDto;

  TRes call({
    String? commissionId,
    String? clientId,
    String? retailerId,
    Input$UpdateIncomingCommissionDto? incoming,
    Input$UpdateOutgoingCommissionDto? outgoing,
    String? startDate,
    String? endDate,
    String? description,
    String? warning,
  });
  CopyWith$Input$UpdateIncomingCommissionDto<TRes> get incoming;
  CopyWith$Input$UpdateOutgoingCommissionDto<TRes> get outgoing;
}

class _CopyWithImpl$Input$UpdateCommissionDto<TRes>
    implements CopyWith$Input$UpdateCommissionDto<TRes> {
  _CopyWithImpl$Input$UpdateCommissionDto(
    this._instance,
    this._then,
  );

  final Input$UpdateCommissionDto _instance;

  final TRes Function(Input$UpdateCommissionDto) _then;

  static const _undefined = <dynamic, dynamic>{};

  TRes call({
    Object? commissionId = _undefined,
    Object? clientId = _undefined,
    Object? retailerId = _undefined,
    Object? incoming = _undefined,
    Object? outgoing = _undefined,
    Object? startDate = _undefined,
    Object? endDate = _undefined,
    Object? description = _undefined,
    Object? warning = _undefined,
  }) =>
      _then(Input$UpdateCommissionDto._({
        ..._instance._$data,
        if (commissionId != _undefined && commissionId != null)
          'commissionId': (commissionId as String),
        if (clientId != _undefined && clientId != null)
          'clientId': (clientId as String),
        if (retailerId != _undefined) 'retailerId': (retailerId as String?),
        if (incoming != _undefined)
          'incoming': (incoming as Input$UpdateIncomingCommissionDto?),
        if (outgoing != _undefined)
          'outgoing': (outgoing as Input$UpdateOutgoingCommissionDto?),
        if (startDate != _undefined) 'startDate': (startDate as String?),
        if (endDate != _undefined) 'endDate': (endDate as String?),
        if (description != _undefined) 'description': (description as String?),
        if (warning != _undefined) 'warning': (warning as String?),
      }));

  CopyWith$Input$UpdateIncomingCommissionDto<TRes> get incoming {
    final local$incoming = _instance.incoming;
    return local$incoming == null
        ? CopyWith$Input$UpdateIncomingCommissionDto.stub(_then(_instance))
        : CopyWith$Input$UpdateIncomingCommissionDto(
            local$incoming, (e) => call(incoming: e));
  }

  CopyWith$Input$UpdateOutgoingCommissionDto<TRes> get outgoing {
    final local$outgoing = _instance.outgoing;
    return local$outgoing == null
        ? CopyWith$Input$UpdateOutgoingCommissionDto.stub(_then(_instance))
        : CopyWith$Input$UpdateOutgoingCommissionDto(
            local$outgoing, (e) => call(outgoing: e));
  }
}

class _CopyWithStubImpl$Input$UpdateCommissionDto<TRes>
    implements CopyWith$Input$UpdateCommissionDto<TRes> {
  _CopyWithStubImpl$Input$UpdateCommissionDto(this._res);

  TRes _res;

  call({
    String? commissionId,
    String? clientId,
    String? retailerId,
    Input$UpdateIncomingCommissionDto? incoming,
    Input$UpdateOutgoingCommissionDto? outgoing,
    String? startDate,
    String? endDate,
    String? description,
    String? warning,
  }) =>
      _res;

  CopyWith$Input$UpdateIncomingCommissionDto<TRes> get incoming =>
      CopyWith$Input$UpdateIncomingCommissionDto.stub(_res);

  CopyWith$Input$UpdateOutgoingCommissionDto<TRes> get outgoing =>
      CopyWith$Input$UpdateOutgoingCommissionDto.stub(_res);
}

class Input$UpdateIncomingCommissionDto {
  factory Input$UpdateIncomingCommissionDto({
    required Enum$CommissionType type,
    required double from,
    double? to,
  }) =>
      Input$UpdateIncomingCommissionDto._({
        r'type': type,
        r'from': from,
        if (to != null) r'to': to,
      });

  Input$UpdateIncomingCommissionDto._(this._$data);

  factory Input$UpdateIncomingCommissionDto.fromJson(
      Map<String, dynamic> data) {
    final result$data = <String, dynamic>{};
    final l$type = data['type'];
    result$data['type'] = fromJson$Enum$CommissionType((l$type as String));
    final l$from = data['from'];
    result$data['from'] = (l$from as num).toDouble();
    if (data.containsKey('to')) {
      final l$to = data['to'];
      result$data['to'] = (l$to as num?)?.toDouble();
    }
    return Input$UpdateIncomingCommissionDto._(result$data);
  }

  Map<String, dynamic> _$data;

  Enum$CommissionType get type => (_$data['type'] as Enum$CommissionType);

  double get from => (_$data['from'] as double);

  double? get to => (_$data['to'] as double?);

  Map<String, dynamic> toJson() {
    final result$data = <String, dynamic>{};
    final l$type = type;
    result$data['type'] = toJson$Enum$CommissionType(l$type);
    final l$from = from;
    result$data['from'] = l$from;
    if (_$data.containsKey('to')) {
      final l$to = to;
      result$data['to'] = l$to;
    }
    return result$data;
  }

  CopyWith$Input$UpdateIncomingCommissionDto<Input$UpdateIncomingCommissionDto>
      get copyWith => CopyWith$Input$UpdateIncomingCommissionDto(
            this,
            (i) => i,
          );

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) {
      return true;
    }
    if (!(other is Input$UpdateIncomingCommissionDto) ||
        runtimeType != other.runtimeType) {
      return false;
    }
    final l$type = type;
    final lOther$type = other.type;
    if (l$type != lOther$type) {
      return false;
    }
    final l$from = from;
    final lOther$from = other.from;
    if (l$from != lOther$from) {
      return false;
    }
    final l$to = to;
    final lOther$to = other.to;
    if (_$data.containsKey('to') != other._$data.containsKey('to')) {
      return false;
    }
    if (l$to != lOther$to) {
      return false;
    }
    return true;
  }

  @override
  int get hashCode {
    final l$type = type;
    final l$from = from;
    final l$to = to;
    return Object.hashAll([
      l$type,
      l$from,
      _$data.containsKey('to') ? l$to : const {},
    ]);
  }
}

abstract class CopyWith$Input$UpdateIncomingCommissionDto<TRes> {
  factory CopyWith$Input$UpdateIncomingCommissionDto(
    Input$UpdateIncomingCommissionDto instance,
    TRes Function(Input$UpdateIncomingCommissionDto) then,
  ) = _CopyWithImpl$Input$UpdateIncomingCommissionDto;

  factory CopyWith$Input$UpdateIncomingCommissionDto.stub(TRes res) =
      _CopyWithStubImpl$Input$UpdateIncomingCommissionDto;

  TRes call({
    Enum$CommissionType? type,
    double? from,
    double? to,
  });
}

class _CopyWithImpl$Input$UpdateIncomingCommissionDto<TRes>
    implements CopyWith$Input$UpdateIncomingCommissionDto<TRes> {
  _CopyWithImpl$Input$UpdateIncomingCommissionDto(
    this._instance,
    this._then,
  );

  final Input$UpdateIncomingCommissionDto _instance;

  final TRes Function(Input$UpdateIncomingCommissionDto) _then;

  static const _undefined = <dynamic, dynamic>{};

  TRes call({
    Object? type = _undefined,
    Object? from = _undefined,
    Object? to = _undefined,
  }) =>
      _then(Input$UpdateIncomingCommissionDto._({
        ..._instance._$data,
        if (type != _undefined && type != null)
          'type': (type as Enum$CommissionType),
        if (from != _undefined && from != null) 'from': (from as double),
        if (to != _undefined) 'to': (to as double?),
      }));
}

class _CopyWithStubImpl$Input$UpdateIncomingCommissionDto<TRes>
    implements CopyWith$Input$UpdateIncomingCommissionDto<TRes> {
  _CopyWithStubImpl$Input$UpdateIncomingCommissionDto(this._res);

  TRes _res;

  call({
    Enum$CommissionType? type,
    double? from,
    double? to,
  }) =>
      _res;
}

class Input$UpdateOutgoingCommissionDto {
  factory Input$UpdateOutgoingCommissionDto({
    required Enum$CommissionType type,
    required double from,
  }) =>
      Input$UpdateOutgoingCommissionDto._({
        r'type': type,
        r'from': from,
      });

  Input$UpdateOutgoingCommissionDto._(this._$data);

  factory Input$UpdateOutgoingCommissionDto.fromJson(
      Map<String, dynamic> data) {
    final result$data = <String, dynamic>{};
    final l$type = data['type'];
    result$data['type'] = fromJson$Enum$CommissionType((l$type as String));
    final l$from = data['from'];
    result$data['from'] = (l$from as num).toDouble();
    return Input$UpdateOutgoingCommissionDto._(result$data);
  }

  Map<String, dynamic> _$data;

  Enum$CommissionType get type => (_$data['type'] as Enum$CommissionType);

  double get from => (_$data['from'] as double);

  Map<String, dynamic> toJson() {
    final result$data = <String, dynamic>{};
    final l$type = type;
    result$data['type'] = toJson$Enum$CommissionType(l$type);
    final l$from = from;
    result$data['from'] = l$from;
    return result$data;
  }

  CopyWith$Input$UpdateOutgoingCommissionDto<Input$UpdateOutgoingCommissionDto>
      get copyWith => CopyWith$Input$UpdateOutgoingCommissionDto(
            this,
            (i) => i,
          );

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) {
      return true;
    }
    if (!(other is Input$UpdateOutgoingCommissionDto) ||
        runtimeType != other.runtimeType) {
      return false;
    }
    final l$type = type;
    final lOther$type = other.type;
    if (l$type != lOther$type) {
      return false;
    }
    final l$from = from;
    final lOther$from = other.from;
    if (l$from != lOther$from) {
      return false;
    }
    return true;
  }

  @override
  int get hashCode {
    final l$type = type;
    final l$from = from;
    return Object.hashAll([
      l$type,
      l$from,
    ]);
  }
}

abstract class CopyWith$Input$UpdateOutgoingCommissionDto<TRes> {
  factory CopyWith$Input$UpdateOutgoingCommissionDto(
    Input$UpdateOutgoingCommissionDto instance,
    TRes Function(Input$UpdateOutgoingCommissionDto) then,
  ) = _CopyWithImpl$Input$UpdateOutgoingCommissionDto;

  factory CopyWith$Input$UpdateOutgoingCommissionDto.stub(TRes res) =
      _CopyWithStubImpl$Input$UpdateOutgoingCommissionDto;

  TRes call({
    Enum$CommissionType? type,
    double? from,
  });
}

class _CopyWithImpl$Input$UpdateOutgoingCommissionDto<TRes>
    implements CopyWith$Input$UpdateOutgoingCommissionDto<TRes> {
  _CopyWithImpl$Input$UpdateOutgoingCommissionDto(
    this._instance,
    this._then,
  );

  final Input$UpdateOutgoingCommissionDto _instance;

  final TRes Function(Input$UpdateOutgoingCommissionDto) _then;

  static const _undefined = <dynamic, dynamic>{};

  TRes call({
    Object? type = _undefined,
    Object? from = _undefined,
  }) =>
      _then(Input$UpdateOutgoingCommissionDto._({
        ..._instance._$data,
        if (type != _undefined && type != null)
          'type': (type as Enum$CommissionType),
        if (from != _undefined && from != null) 'from': (from as double),
      }));
}

class _CopyWithStubImpl$Input$UpdateOutgoingCommissionDto<TRes>
    implements CopyWith$Input$UpdateOutgoingCommissionDto<TRes> {
  _CopyWithStubImpl$Input$UpdateOutgoingCommissionDto(this._res);

  TRes _res;

  call({
    Enum$CommissionType? type,
    double? from,
  }) =>
      _res;
}

class Input$DeleteCommissionDto {
  factory Input$DeleteCommissionDto({
    required String clientId,
    required List<String?> commissionIds,
  }) =>
      Input$DeleteCommissionDto._({
        r'clientId': clientId,
        r'commissionIds': commissionIds,
      });

  Input$DeleteCommissionDto._(this._$data);

  factory Input$DeleteCommissionDto.fromJson(Map<String, dynamic> data) {
    final result$data = <String, dynamic>{};
    final l$clientId = data['clientId'];
    result$data['clientId'] = (l$clientId as String);
    final l$commissionIds = data['commissionIds'];
    result$data['commissionIds'] =
        (l$commissionIds as List<dynamic>).map((e) => (e as String?)).toList();
    return Input$DeleteCommissionDto._(result$data);
  }

  Map<String, dynamic> _$data;

  String get clientId => (_$data['clientId'] as String);

  List<String?> get commissionIds => (_$data['commissionIds'] as List<String?>);

  Map<String, dynamic> toJson() {
    final result$data = <String, dynamic>{};
    final l$clientId = clientId;
    result$data['clientId'] = l$clientId;
    final l$commissionIds = commissionIds;
    result$data['commissionIds'] = l$commissionIds.map((e) => e).toList();
    return result$data;
  }

  CopyWith$Input$DeleteCommissionDto<Input$DeleteCommissionDto> get copyWith =>
      CopyWith$Input$DeleteCommissionDto(
        this,
        (i) => i,
      );

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) {
      return true;
    }
    if (!(other is Input$DeleteCommissionDto) ||
        runtimeType != other.runtimeType) {
      return false;
    }
    final l$clientId = clientId;
    final lOther$clientId = other.clientId;
    if (l$clientId != lOther$clientId) {
      return false;
    }
    final l$commissionIds = commissionIds;
    final lOther$commissionIds = other.commissionIds;
    if (l$commissionIds.length != lOther$commissionIds.length) {
      return false;
    }
    for (int i = 0; i < l$commissionIds.length; i++) {
      final l$commissionIds$entry = l$commissionIds[i];
      final lOther$commissionIds$entry = lOther$commissionIds[i];
      if (l$commissionIds$entry != lOther$commissionIds$entry) {
        return false;
      }
    }
    return true;
  }

  @override
  int get hashCode {
    final l$clientId = clientId;
    final l$commissionIds = commissionIds;
    return Object.hashAll([
      l$clientId,
      Object.hashAll(l$commissionIds.map((v) => v)),
    ]);
  }
}

abstract class CopyWith$Input$DeleteCommissionDto<TRes> {
  factory CopyWith$Input$DeleteCommissionDto(
    Input$DeleteCommissionDto instance,
    TRes Function(Input$DeleteCommissionDto) then,
  ) = _CopyWithImpl$Input$DeleteCommissionDto;

  factory CopyWith$Input$DeleteCommissionDto.stub(TRes res) =
      _CopyWithStubImpl$Input$DeleteCommissionDto;

  TRes call({
    String? clientId,
    List<String?>? commissionIds,
  });
}

class _CopyWithImpl$Input$DeleteCommissionDto<TRes>
    implements CopyWith$Input$DeleteCommissionDto<TRes> {
  _CopyWithImpl$Input$DeleteCommissionDto(
    this._instance,
    this._then,
  );

  final Input$DeleteCommissionDto _instance;

  final TRes Function(Input$DeleteCommissionDto) _then;

  static const _undefined = <dynamic, dynamic>{};

  TRes call({
    Object? clientId = _undefined,
    Object? commissionIds = _undefined,
  }) =>
      _then(Input$DeleteCommissionDto._({
        ..._instance._$data,
        if (clientId != _undefined && clientId != null)
          'clientId': (clientId as String),
        if (commissionIds != _undefined && commissionIds != null)
          'commissionIds': (commissionIds as List<String?>),
      }));
}

class _CopyWithStubImpl$Input$DeleteCommissionDto<TRes>
    implements CopyWith$Input$DeleteCommissionDto<TRes> {
  _CopyWithStubImpl$Input$DeleteCommissionDto(this._res);

  TRes _res;

  call({
    String? clientId,
    List<String?>? commissionIds,
  }) =>
      _res;
}

enum Enum$CommissionType { percent, currency, $unknown }

String toJson$Enum$CommissionType(Enum$CommissionType e) {
  switch (e) {
    case Enum$CommissionType.percent:
      return r'percent';
    case Enum$CommissionType.currency:
      return r'currency';
    case Enum$CommissionType.$unknown:
      return r'$unknown';
  }
}

Enum$CommissionType fromJson$Enum$CommissionType(String value) {
  switch (value) {
    case r'percent':
      return Enum$CommissionType.percent;
    case r'currency':
      return Enum$CommissionType.currency;
    default:
      return Enum$CommissionType.$unknown;
  }
}

enum Enum$__TypeKind {
  SCALAR,
  OBJECT,
  INTERFACE,
  UNION,
  ENUM,
  INPUT_OBJECT,
  LIST,
  NON_NULL,
  $unknown
}

String toJson$Enum$__TypeKind(Enum$__TypeKind e) {
  switch (e) {
    case Enum$__TypeKind.SCALAR:
      return r'SCALAR';
    case Enum$__TypeKind.OBJECT:
      return r'OBJECT';
    case Enum$__TypeKind.INTERFACE:
      return r'INTERFACE';
    case Enum$__TypeKind.UNION:
      return r'UNION';
    case Enum$__TypeKind.ENUM:
      return r'ENUM';
    case Enum$__TypeKind.INPUT_OBJECT:
      return r'INPUT_OBJECT';
    case Enum$__TypeKind.LIST:
      return r'LIST';
    case Enum$__TypeKind.NON_NULL:
      return r'NON_NULL';
    case Enum$__TypeKind.$unknown:
      return r'$unknown';
  }
}

Enum$__TypeKind fromJson$Enum$__TypeKind(String value) {
  switch (value) {
    case r'SCALAR':
      return Enum$__TypeKind.SCALAR;
    case r'OBJECT':
      return Enum$__TypeKind.OBJECT;
    case r'INTERFACE':
      return Enum$__TypeKind.INTERFACE;
    case r'UNION':
      return Enum$__TypeKind.UNION;
    case r'ENUM':
      return Enum$__TypeKind.ENUM;
    case r'INPUT_OBJECT':
      return Enum$__TypeKind.INPUT_OBJECT;
    case r'LIST':
      return Enum$__TypeKind.LIST;
    case r'NON_NULL':
      return Enum$__TypeKind.NON_NULL;
    default:
      return Enum$__TypeKind.$unknown;
  }
}

enum Enum$__DirectiveLocation {
  QUERY,
  MUTATION,
  SUBSCRIPTION,
  FIELD,
  FRAGMENT_DEFINITION,
  FRAGMENT_SPREAD,
  INLINE_FRAGMENT,
  VARIABLE_DEFINITION,
  SCHEMA,
  SCALAR,
  OBJECT,
  FIELD_DEFINITION,
  ARGUMENT_DEFINITION,
  INTERFACE,
  UNION,
  ENUM,
  ENUM_VALUE,
  INPUT_OBJECT,
  INPUT_FIELD_DEFINITION,
  $unknown
}

String toJson$Enum$__DirectiveLocation(Enum$__DirectiveLocation e) {
  switch (e) {
    case Enum$__DirectiveLocation.QUERY:
      return r'QUERY';
    case Enum$__DirectiveLocation.MUTATION:
      return r'MUTATION';
    case Enum$__DirectiveLocation.SUBSCRIPTION:
      return r'SUBSCRIPTION';
    case Enum$__DirectiveLocation.FIELD:
      return r'FIELD';
    case Enum$__DirectiveLocation.FRAGMENT_DEFINITION:
      return r'FRAGMENT_DEFINITION';
    case Enum$__DirectiveLocation.FRAGMENT_SPREAD:
      return r'FRAGMENT_SPREAD';
    case Enum$__DirectiveLocation.INLINE_FRAGMENT:
      return r'INLINE_FRAGMENT';
    case Enum$__DirectiveLocation.VARIABLE_DEFINITION:
      return r'VARIABLE_DEFINITION';
    case Enum$__DirectiveLocation.SCHEMA:
      return r'SCHEMA';
    case Enum$__DirectiveLocation.SCALAR:
      return r'SCALAR';
    case Enum$__DirectiveLocation.OBJECT:
      return r'OBJECT';
    case Enum$__DirectiveLocation.FIELD_DEFINITION:
      return r'FIELD_DEFINITION';
    case Enum$__DirectiveLocation.ARGUMENT_DEFINITION:
      return r'ARGUMENT_DEFINITION';
    case Enum$__DirectiveLocation.INTERFACE:
      return r'INTERFACE';
    case Enum$__DirectiveLocation.UNION:
      return r'UNION';
    case Enum$__DirectiveLocation.ENUM:
      return r'ENUM';
    case Enum$__DirectiveLocation.ENUM_VALUE:
      return r'ENUM_VALUE';
    case Enum$__DirectiveLocation.INPUT_OBJECT:
      return r'INPUT_OBJECT';
    case Enum$__DirectiveLocation.INPUT_FIELD_DEFINITION:
      return r'INPUT_FIELD_DEFINITION';
    case Enum$__DirectiveLocation.$unknown:
      return r'$unknown';
  }
}

Enum$__DirectiveLocation fromJson$Enum$__DirectiveLocation(String value) {
  switch (value) {
    case r'QUERY':
      return Enum$__DirectiveLocation.QUERY;
    case r'MUTATION':
      return Enum$__DirectiveLocation.MUTATION;
    case r'SUBSCRIPTION':
      return Enum$__DirectiveLocation.SUBSCRIPTION;
    case r'FIELD':
      return Enum$__DirectiveLocation.FIELD;
    case r'FRAGMENT_DEFINITION':
      return Enum$__DirectiveLocation.FRAGMENT_DEFINITION;
    case r'FRAGMENT_SPREAD':
      return Enum$__DirectiveLocation.FRAGMENT_SPREAD;
    case r'INLINE_FRAGMENT':
      return Enum$__DirectiveLocation.INLINE_FRAGMENT;
    case r'VARIABLE_DEFINITION':
      return Enum$__DirectiveLocation.VARIABLE_DEFINITION;
    case r'SCHEMA':
      return Enum$__DirectiveLocation.SCHEMA;
    case r'SCALAR':
      return Enum$__DirectiveLocation.SCALAR;
    case r'OBJECT':
      return Enum$__DirectiveLocation.OBJECT;
    case r'FIELD_DEFINITION':
      return Enum$__DirectiveLocation.FIELD_DEFINITION;
    case r'ARGUMENT_DEFINITION':
      return Enum$__DirectiveLocation.ARGUMENT_DEFINITION;
    case r'INTERFACE':
      return Enum$__DirectiveLocation.INTERFACE;
    case r'UNION':
      return Enum$__DirectiveLocation.UNION;
    case r'ENUM':
      return Enum$__DirectiveLocation.ENUM;
    case r'ENUM_VALUE':
      return Enum$__DirectiveLocation.ENUM_VALUE;
    case r'INPUT_OBJECT':
      return Enum$__DirectiveLocation.INPUT_OBJECT;
    case r'INPUT_FIELD_DEFINITION':
      return Enum$__DirectiveLocation.INPUT_FIELD_DEFINITION;
    default:
      return Enum$__DirectiveLocation.$unknown;
  }
}

const possibleTypesMap = <String, Set<String>>{};
