class DeleteCommissionMutation extends GraphQLMutationWrapper<Mutation$DeleteCommission> {
  DeleteCommissionMutation(String clientId, String commissionId) : super(documentNodeMutationDeleteCommission,
      Options$Mutation$DeleteCommission(variables:
        Variables$Mutation$DeleteCommission(deleteCommissionDto:
          Input$DeleteCommissionDto(clientId: clientId,
              commissionIds: [commissionId])
        )
      )
  );
}
