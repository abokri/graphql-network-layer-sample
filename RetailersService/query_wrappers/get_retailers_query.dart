class GetRetailersQuery extends GraphQLQueryWrapper<Query$GetRetailers> {
  GetRetailersQuery(String clientId, String country) : super(documentNodeQueryGetRetailers,
      Options$Query$GetRetailers(variables: Variables$Query$GetRetailers(clientId: clientId, country: country)));
}
