abstract class IRetailersService {
  Future<List<Retailer>> getRetailers(String clientId, String country);
  Future<bool> deleteCommission(String clientId, String commissionId);
}
