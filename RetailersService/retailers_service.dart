class RetailersService implements IRetailersService {
  final INetworkService _networkService;

  RetailersService(this._networkService);

  Future<List<Retailer>> getRetailers(String clientId, String country) async {
    final queryWrapper = GetRetailersQuery(clientId, country);

    final result = await _networkService.query(
      queryWrapper,
    );

    final listRetailers = result.getRetailers;

    // Use null-aware operator and map directly
    return listRetailers?.map((e) =>
        Retailer(country: e?.country,
            logo: e?.logo,
            name: e?.name,
            commissions: e?.commissions.map((t) =>
                Commission(clientId: t.clientId,
                    commissionId: t.commissionId,
                    retailerId: t.retailerId)
            ).toList() ?? [])
    ).toList() ?? [];
  }

  Future<bool> deleteCommission(String clientId, String commissionId) async {
    final query = DeleteCommissionMutation(clientId, commissionId);

    final result = await _networkService.mutation(query);

    return result.deleteCommission ?? false;
  }
}
